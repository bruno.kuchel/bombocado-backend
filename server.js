const express = require('express')
const bodyParser = require('body-parser')
var cors = require('cors')
const connectDB = require('./config/db')
const app = express()
const fileUpload = require('express-fileupload')
const PORT = process.env.PORT || 3001


//init Middleware

app.use(cors())
app.use(express.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())
app.use('/uploads', express.static('uploads'))


//connect Database

connectDB()

app.use(fileUpload({
    createParentPath: true
}))

app.get('/', (req, res) => res.send('APP operativa!'))
app.use('/auth', require('./routes/api/auth/auth'))
app.use('/category', require('./routes/api/admin/category'))
app.use('/product', require('./routes/api/admin/product'))
app.use('/banner', require('./routes/api/admin/banner'))
app.use('/user', require('./routes/api/admin/user'))
app.use('/menu', require ('./routes/api/portal/product'))
app.use('/carousel', require ('./routes/api/portal/carousel'))
app.use('/contact', require ('./routes/api/portal/contact'))




app.listen(PORT, () => {console.log(`APP funcionando na porta ${PORT}`)})