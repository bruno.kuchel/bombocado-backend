const express = require('express')
const Banner = require('../../../models/banner')
const router = express.Router()
const MSGS = require('../../../messages')
const config = require('config')


// @route    GET /banner
// @desc     LIST available banners
// @access   Public

router.get('/', async (req, res, next) => {
  try {
    let banner = await Banner.find({ status: true })
    const BUCKET_PUBLIC_PATH = process.env.BUCKET_PUBLIC_PATH || config.get('BUCKET_PUBLIC_PATH')
    banner = banner.map(function (banner) {
      banner.photo = `${BUCKET_PUBLIC_PATH}${banner.photo}`
      return banner
    })
    res.json(banner)
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})
module.exports = router
