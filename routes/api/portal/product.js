const express = require('express')
const Product = require('../../../models/product')
const { check, validationResult } = require('express-validator')
const router = express.Router()
const MSGS = require('../../../messages')
const config = require('config')


// @route    GET /products
// @desc     LIST available products
// @access   Public

router.get('/', async (req, res, next) => {
  try {
    let products = await Product.find({ status: true })
    const BUCKET_PUBLIC_PATH = process.env.BUCKET_PUBLIC_PATH || config.get('BUCKET_PUBLIC_PATH')
    products = products.map(function (product) {
      product.photo = `${BUCKET_PUBLIC_PATH}${product.photo}`
      return product
    })
    res.json(products)
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})
module.exports = router
