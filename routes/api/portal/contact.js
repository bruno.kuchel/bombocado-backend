const express = require('express')
const router = express.Router()
const MSGS = require('../../../messages')
const sgMail = require('@sendgrid/mail');
const config = require('config')


// @route    POST /contact
// @desc     POST contact form
// @access   Public

router.post('/', async (req, res) => {
    try {
        sgMail.setApiKey(process.env.SENDGRID_API_KEY || config.get("SENDGRID_API_KEY"));
    const msg = {
    to: 'bombocado.food@gmail.com',
    from: 'hola@santofruto.es',
    subject: 'Contacto web Bombocado',
    text: req.body.message,
    html: `<h1>Este es un mensaje de ${req.body.name}</h1>
    <br/>
    <p>Email: ${req.body.email} </p>
    <p>Mensaje: ${req.body.message}</p>`,
    }
    sgMail
    .send(msg)
    res.status(200).send({ "success": 'message sent' })
        
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
      }
        
    })

module.exports = router
