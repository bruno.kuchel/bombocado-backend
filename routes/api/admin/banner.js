const express = require('express')
const Banner = require('../../../models/banner')
const auth = require('../../../middleware/auth')
const { check, validationResult } = require('express-validator')
const router = express.Router()
const MSGS = require('../../../messages')
const file = require('../../../middleware/file')
const config = require('config')


// @route    POST /banner
// @desc     CREATE banner
// @access   Private

router.post('/', auth, file, async (req, res, next) => {
  try {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    } else {
      req.body.photo = `banner/${req.body.photo_name}`
      let banner = new Banner(req.body)
      banner.last_modified_by = req.user.id
      await banner.save()
      if (banner.id) {
        const BUCKET_PUBLIC_PATH = process.env.BUCKET_PUBLIC_PATH || config.get('BUCKET_PUBLIC_PATH')
        banner.photo = `${BUCKET_PUBLIC_PATH}${banner.photo}`
        res.json(banner)        
      }
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})



// @route    GET /banner
// @desc     LIST banner
// @access   Private

router.get('/', auth, async (req, res, next) => {
  try {
    let banner = await Banner.find({})
    const BUCKET_PUBLIC_PATH = process.env.BUCKET_PUBLIC_PATH || config.get('BUCKET_PUBLIC_PATH')
    banner = banner.map(function (banner) {
      banner.photo = `${BUCKET_PUBLIC_PATH}${banner.photo}`
      return banner
    })
    res.json(banner)
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})



// @route    GET /banner/:id
// @desc     DETAIL banner
// @access   Public

router.get('/:id', [], async (req, res, next) => {
  try {
    const id = req.params.id
    const banner = await Banner.findOne({ _id: id })
    if (banner) {
      res.json(banner)
    } else {
      res.status(404).send({ "error": MSGS.PRODUCT404 })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})


// @route    PATCH /banner/:id
// @desc     PARTIAL UPDATE banner
// @access   Private

router.patch('/:id', auth, file, async (req, res, next) => {
  try {
    req.body.last_modified_by = req.user.id
    if (req.body.photo_name) {
      req.body.photo = `banner/${req.body.photo_name}`
      
    }
    const banner = await Banner.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true })
    if (banner) {
      res.json(banner)
    } else {
      res.status(404).send({ "error": MSGS.PRODUCT404 })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})



// @route    DELETE /banner/:id
// @desc     DELETE banner
// @access   Private

router.delete('/:id', auth, async (req, res, next) => {
  try {
    const id = req.params.id
    const banner = await Banner.findOneAndDelete({ _id: id })
    if (banner) {
      res.json(banner)
    } else {
      res.status(404).send({ "error": MSGS.PRODUCT404 })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})


module.exports = router
