const MSGS = {

    'GENERIC_ERROR': 'Se ha producido un error',

    //AUTH
    'USER404' : 'Usuario no encontrado',
    'INVALID_EMAIL' : 'El e-mail no es válido',
    'PASSWORD_REQUIRED' : 'La contraseña es obligatoria',
    'INVALID_PASSWORD' : 'La contraseña no es válida',
    'NO_TOKEN' : 'Token no enviado',
    'INVALID_TOKEN' : 'Token no válido',

    //CATEGORY

    'CATEGORY404' : 'Categoría no encontrada',
    'CANTDELETECATEGORY': 'Categoría con productos asociados',

    // PRODUCT

    'PRODUCT404' : 'Producto no encontrado',

    //USER

    'EMAIL_REQUIRED' : 'El e-mail es obligatorio',
    'NAME_REQUIRED' : 'El nombre es obligatorio',
    'SURNAME_REQUIRED' : 'El apellido es obligatorio',
    'PASSWORD_LENGHT' : 'El email no es válido',

    //CONTENT

    'CONTENT404' : 'Contenido no encontrado',

   //FILE
   'FILE_NOT_SENT': 'Archivo no enviado',
   'FILE_UPLOADED' : 'Archivo enviado correctamente',
   'FILE_INVALID_FORMAT' : 'El formato del archivo no es válido'
}

module.exports = MSGS
