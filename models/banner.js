const mongoose = require('mongoose')

const BannerSchema = new mongoose.Schema({
    photo: {
        type : String        
    },
    title: {
        type : String,
        required : true
    },
    
    description: {
        type : String,
        required : true
    },
    
    last_modified_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    last_modification_date: {
        type: Date,
        default: Date.now
    },
    status: {
        type : Boolean,
        default: false
        
    }
}, { autoCreate : true })

module.exports = mongoose.model('banner', BannerSchema)
